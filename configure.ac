dnl ##
dnl ##  OSSP sa - Socket Abstraction
dnl ##  Copyright (c) 2001-2006 Ralf S. Engelschall <rse@engelschall.com>
dnl ##  Copyright (c) 2001-2006 The OSSP Project <http://www.ossp.org/>
dnl ##  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
dnl ##
dnl ##  This file is part of OSSP sa, a socket abstraction library which
dnl ##  can be found at http://www.ossp.org/pkg/lib/sa/.
dnl ##
dnl ##  Permission to use, copy, modify, and distribute this software for
dnl ##  any purpose with or without fee is hereby granted, provided that
dnl ##  the above copyright notice and this permission notice appear in all
dnl ##  copies.
dnl ##
dnl ##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
dnl ##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
dnl ##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
dnl ##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
dnl ##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
dnl ##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
dnl ##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
dnl ##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
dnl ##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
dnl ##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
dnl ##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
dnl ##  SUCH DAMAGE.
dnl ##
dnl ##  configure.ac: GNU Autoconf source script
dnl ##

AC_PREREQ(2.53)
AC_INIT
SA_VERSION_STR=`./shtool version -l txt -d long VERSION`
SA_VERSION_RAW=`./shtool version -l txt -d short VERSION`
./shtool echo -e "Configuring %BOSSP sa%b (Socket Abstraction), version %B${SA_VERSION_STR}%b"
AC_SUBST(SA_VERSION_STR)
AC_SUBST(SA_VERSION_RAW)

AC_PROG_MAKE_SET
AC_PROG_CC
AC_CHECK_DEBUGGING

sinclude(libtool.m4)
AC_PROG_LIBTOOL

sinclude(sa.ac)
SA_CHECK_ALL

AC_CHECK_EXTLIB([OSSP ex], ex, __ex_ctx, ex.h,
                [AC_DEFINE(WITH_EX, 1, [Define to 1 if building with OSSP ex])])

AC_CONFIG_HEADERS(config.h)
AC_CONFIG_FILES([Makefile sa-config sa.pc])
AC_CONFIG_COMMANDS([adjustment], [chmod a+x sa-config])
AC_OUTPUT

