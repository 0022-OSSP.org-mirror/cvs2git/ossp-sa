dnl ##
dnl ##  OSSP sa - Socket Abstraction
dnl ##  Copyright (c) 2001-2006 Ralf S. Engelschall <rse@engelschall.com>
dnl ##  Copyright (c) 2001-2006 The OSSP Project <http://www.ossp.org/>
dnl ##  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
dnl ##
dnl ##  This file is part of OSSP sa, a socket abstraction library which
dnl ##  can be found at http://www.ossp.org/pkg/lib/sa/.
dnl ##
dnl ##  Permission to use, copy, modify, and distribute this software for
dnl ##  any purpose with or without fee is hereby granted, provided that
dnl ##  the above copyright notice and this permission notice appear in all
dnl ##  copies.
dnl ##
dnl ##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
dnl ##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
dnl ##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
dnl ##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
dnl ##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
dnl ##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
dnl ##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
dnl ##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
dnl ##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
dnl ##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
dnl ##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
dnl ##  SUCH DAMAGE.
dnl ##
dnl ##  sa.ac: socket abstraction Autoconf checks
dnl ##

dnl #   Check for an ANSI C typedef in a header
dnl #   configure.in:
dnl #     SA_CHECK_TYPEDEF(<typedef>, <header>)
dnl #   acconfig.h:
dnl #     #undef HAVE_<typedef>

AC_DEFUN(SA_CHECK_TYPEDEF,[dnl
    AC_REQUIRE([AC_HEADER_STDC])dnl
    AC_MSG_CHECKING(for typedef $1)
    AC_CACHE_VAL(ac_cv_typedef_$1,[
        AC_EGREP_CPP(dnl
changequote(<<,>>)dnl
<<(^|[^a-zA-Z_0-9])$1[^a-zA-Z_0-9]>>dnl
changequote([,]), [
#include <$2>],
            ac_cv_typedef_$1=yes,
            ac_cv_typedef_$1=no
        )
    ])dnl
    AC_MSG_RESULT($ac_cv_typedef_$1)
    if test $ac_cv_typedef_$1 = yes; then
        AC_DEFINE(HAVE_[]translit($1, [a-z], [A-Z]), 1, [Define to 1 if $1 exists])
    fi
])

dnl #  Check whether to use SO_RCVTIMEO|SO_SNDTIMEO with setsockopt(2)
dnl #   configure.ac:
dnl #     SA_CHECK_SOCKOPT(SO_RCVTIMEO)
dnl #     SA_CHECK_SOCKOPT(SO_SNDTIMEO)
dnl #   config.h:
dnl #     #undef USE_SO_RCVTIMEO or #define USE_SO_RCVTIMEO 1
dnl #     #undef USE_SO_SNDTIMEO or #define USE_SO_SNDTIMEO 1

AC_DEFUN(SA_CHECK_SOCKOPT, [
AC_MSG_CHECKING(whether to use $1 with setsockopt(2))
AC_TRY_RUN([
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>

int main(void)
{
    int s;
    struct timeval timeo;
    timeo.tv_sec  = 3;
    timeo.tv_usec = 3;

#ifndef $1
    exit(3);
#else
    if ((s = socket(PF_INET, SOCK_STREAM, 0)) == -1)
        exit(2);

    /* fails on Solaris 2.6,8,9,10 and Debian 2.2 because
       SO_RCVTIMEO|SO_SNDTIMEO are defined but not implemented */
    if (setsockopt(s, SOL_SOCKET, $1, (void *)&timeo, sizeof(timeo)) == -1)
        exit(1);

    exit(0);
#endif
}
]
, [ AC_MSG_RESULT([yes]) AC_DEFINE(USE_$1, 1, [Define to use $1 with setsockopt(2)]) ]
, [ AC_MSG_RESULT([no]) ]
)
])dnl

dnl ##
dnl ##  Check for C99 va_copy() implementation
dnl ##  (and provide fallback implementation if neccessary)
dnl ##
dnl ##  configure.in:
dnl ##    SA_CHECK_VA_COPY
dnl ##  foo.c:
dnl ##    #include "config.h"
dnl ##    [...]
dnl ##    va_copy(d,s)
dnl ##
dnl ##  This check is rather complex: first because we really have to
dnl ##  try various possible implementations in sequence and second, we
dnl ##  cannot define a macro in config.h with parameters directly.
dnl ##

dnl #   test program for va_copy() implementation
changequote(<<,>>)
m4_define(__va_copy_test, <<[
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#define DO_VA_COPY(d, s) $1
void test(char *str, ...)
{
    va_list ap, ap2;
    int i;
    va_start(ap, str);
    DO_VA_COPY(ap2, ap);
    for (i = 1; i <= 9; i++) {
        int k = (int)va_arg(ap, int);
        if (k != i)
            abort();
    }
    DO_VA_COPY(ap, ap2);
    for (i = 1; i <= 9; i++) {
        int k = (int)va_arg(ap, int);
        if (k != i)
            abort();
    }
    va_end(ap);
}
int main(int argc, char *argv[])
{
    test("test", 1, 2, 3, 4, 5, 6, 7, 8, 9);
    exit(0);
}
]>>)
changequote([,])

dnl #   test driver for va_copy() implementation
m4_define(__va_copy_check, [
    AH_VERBATIM($1,
[/* Predefined possible va_copy() implementation (id: $1) */
#define __VA_COPY_USE_$1(d, s) $2])
    if test ".$ac_cv_va_copy" = .; then
        AC_TRY_RUN(__va_copy_test($2), [ac_cv_va_copy="$1"])
    fi
])

dnl #   Autoconf check for va_copy() implementation checking
AC_DEFUN(SA_CHECK_VA_COPY,[
  dnl #   provide Autoconf display check message
  AC_MSG_CHECKING(for va_copy() function)
  dnl #   check for various implementations in priorized sequence   
  AC_CACHE_VAL(ac_cv_va_copy, [
    ac_cv_va_copy=""
    dnl #   1. check for standardized C99 macro
    __va_copy_check(C99, [va_copy((d), (s))])
    dnl #   2. check for alternative/deprecated GCC macro
    __va_copy_check(GCM, [VA_COPY((d), (s))])
    dnl #   3. check for internal GCC macro (high-level define)
    __va_copy_check(GCH, [__va_copy((d), (s))])
    dnl #   4. check for internal GCC macro (built-in function)
    __va_copy_check(GCB, [__builtin_va_copy((d), (s))])
    dnl #   5. check for assignment approach (assuming va_list is a struct)
    __va_copy_check(ASS, [do { (d) = (s); } while (0)])
    dnl #   6. check for assignment approach (assuming va_list is a pointer)
    __va_copy_check(ASP, [do { *(d) = *(s); } while (0)])
    dnl #   7. check for memory copying approach (assuming va_list is a struct)
    __va_copy_check(CPS, [memcpy((void *)&(d), (void *)&(s)), sizeof((s))])
    dnl #   8. check for memory copying approach (assuming va_list is a pointer)
    __va_copy_check(CPP, [memcpy((void *)(d), (void *)(s)), sizeof(*(s))])
    if test ".$ac_cv_va_copy" = .; then
        AC_ERROR([no working implementation found])
    fi
  ])
  dnl #   optionally activate the fallback implementation
  if test ".$ac_cv_va_copy" = ".C99"; then
      AC_DEFINE(HAVE_VA_COPY, 1, [Define if va_copy() macro exists (and no fallback implementation is required)])
  fi
  dnl #   declare which fallback implementation to actually use
  AC_DEFINE_UNQUOTED([__VA_COPY_USE], [__VA_COPY_USE_$ac_cv_va_copy],
      [Define to id of used va_copy() implementation])
  dnl #   provide activation hook for fallback implementation
  AH_VERBATIM([__VA_COPY_ACTIVATION],
[/* Optional va_copy() implementation activation */
#ifndef HAVE_VA_COPY
#define va_copy(d, s) __VA_COPY_USE(d, s)
#endif
])
  dnl #   provide Autoconf display result message
  if test ".$ac_cv_va_copy" = ".C99"; then
      AC_MSG_RESULT([yes])
  else
      AC_MSG_RESULT([no (using fallback implementation)])
  fi
])


dnl #   Check for anything OSSP SA wants to know
dnl #   configure.in:
dnl #     SA_CHECK_ALL

AC_DEFUN(SA_CHECK_ALL,[
    #   make sure libnsl and libsocket are linked in if they exist
    AC_CHECK_LIB(nsl, gethostname)
    if test ".`echo $LIBS | grep nsl`" = .; then
        AC_CHECK_LIB(nsl, gethostbyname)
    fi
    AC_CHECK_LIB(socket, accept)

    #   make sure some platforms find their IPv6 library
    AC_CHECK_LIB(inet6, getaddrinfo)

    #   check for system headers
    AC_CHECK_HEADERS(string.h sys/types.h sys/socket.h netdb.h netinet/in.h)

    #   check for system functions
    AC_CHECK_FUNCS(inet_addr inet_aton inet_pton inet_ntoa inet_ntop snprintf getaddrinfo)

    dnl # check for network/socket size type
    SA_CHECK_TYPEDEF(socklen_t, sys/socket.h)
    SA_CHECK_TYPEDEF(ssize_t, sys/types.h)
    SA_CHECK_SOCKOPT(SO_RCVTIMEO)
    SA_CHECK_SOCKOPT(SO_SNDTIMEO)

    dnl # check for va_copy()
    SA_CHECK_VA_COPY
])

