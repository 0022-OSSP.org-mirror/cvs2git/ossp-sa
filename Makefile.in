##
##  OSSP sa - Socket Abstraction
##  Copyright (c) 2001-2006 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2001-2006 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP sa, a socket abstraction library which
##  can be found at http://www.ossp.org/pkg/lib/sa/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  Makefile.in: make(1) build procedure
##

@SET_MAKE@

DESTDIR     =
prefix      = @prefix@
exec_prefix = @exec_prefix@
bindir      = @bindir@
libdir      = @libdir@
includedir  = @includedir@
mandir      = @mandir@

CC          = @CC@
CPPFLAGS    = @CPPFLAGS@
CFLAGS      = @DEFS@ @CFLAGS@
LDFLAGS     = @LDFLAGS@
LIBS        = @LIBS@
RM          = rm -f
RMDIR       = rmdir
SHTOOL      = ./shtool
LIBTOOL     = ./libtool
TRUE        = true
POD2MAN     = pod2man

LIB_NAME    = libsa.la
LIB_OBJS    = sa.lo

TST_NAME    = sa_test
TST_OBJS    = sa_test.o ts.o

.SUFFIXES:
.SUFFIXES: .c .o .lo

all: $(LIB_NAME) $(TST_NAME)

.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

.c.lo:
	@$(LIBTOOL) --mode=compile $(CC) $(CPPFLAGS) $(CFLAGS) -c $<

$(LIB_NAME): $(LIB_OBJS)
	@$(LIBTOOL) --mode=link $(CC) -o $(LIB_NAME) $(LIB_OBJS) -rpath $(libdir) \
	    -version-info `$(SHTOOL) version -l txt -d libtool VERSION`

$(TST_NAME): $(TST_OBJS) $(LIB_NAME)
	@$(LIBTOOL) --mode=link $(CC) $(LDFLAGS) -o $(TST_NAME) $(TST_OBJS) $(LIB_NAME) $(LIBS)

man: sa.3
sa.3: sa.pod
	V1=`$(SHTOOL) version -l txt -d short VERSION`; \
	V2=`$(SHTOOL) version -l txt -d long VERSION`; \
	D=`$(SHTOOL) version -l txt -d long VERSION | sed -e 's;.*(;;' -e 's;).*;;'`; \
	$(POD2MAN) --quotes=none \
	           --section=3 --center="Socket Abstraction" \
	           --release="$$D" --date="OSSP sa $$V1" sa.pod | \
	sed -e "s;SA_VERSION_STR;$$V2;" >sa.3

check: $(TST_NAME)
	@$(LIBTOOL) --mode=execute ./$(TST_NAME)

install:
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(prefix)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(bindir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(includedir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(libdir)/pkgconfig
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(mandir)/man3
	$(SHTOOL) install -c -m 755 sa-config $(DESTDIR)$(bindir)/
	$(SHTOOL) install -c -m 644 sa.pc $(DESTDIR)$(libdir)/pkgconfig/
	$(SHTOOL) install -c -m 644 sa.h $(DESTDIR)$(includedir)/
	$(SHTOOL) install -c -m 644 sa.3 $(DESTDIR)$(mandir)/man3/
	@$(LIBTOOL) --mode=install $(SHTOOL) install -c -m 644 libsa.la $(DESTDIR)$(libdir)/

uninstall:
	@$(LIBTOOL) --mode=uninstall $(RM) $(DESTDIR)$(libdir)/libsa.la
	-$(RM) $(DESTDIR)$(mandir)/man3/sa.3
	-$(RM) $(DESTDIR)$(includedir)/sa.h
	-$(RM) $(DESTDIR)$(libdir)/pkgconfig/sa.pc
	-$(RM) $(DESTDIR)$(bindir)/sa-config
	-$(RMDIR) $(DESTDIR)$(mandir)/man3 >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(mandir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(libdir)/pkgconfig >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(libdir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(includedir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(bindir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(prefix) >/dev/null 2>&1 || $(TRUE)

clean:
	-$(RM) $(LIB_NAME) $(LIB_OBJS)
	-$(RM) $(TST_NAME) $(TST_OBJS)
	-$(RM) -r .libs >/dev/null 2>&1 || $(TRUE)
	-$(RM) *.o *.lo

distclean: clean
	-$(RM) config.log config.status config.cache
	-$(RM) Makefile config.h sa-config sa.pc
	-$(RM) libtool

realclean: distclean
	-$(RM) sa.3
	-$(RM) configure config.h.in
	-$(RM) shtool
	-$(RM) ltmain.sh libtool.m4 config.guess config.sub

