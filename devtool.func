##
##  devtool.func -- Development Tool Functions
##  Copyright (c) 2001-2004 Ralf S. Engelschall <rse@engelschall.com>
##

devtool_require () {
    t="$1"; o="$2"; p="$3"; e="$4"; a="$5"
    v=`($t $o | head -1 | awk "{ print \\\$$p; }") 2>/dev/null`
    if [ ".$v" = . ]; then
        echo "devtool:ERROR: unable to determine version of $t" 1>&2
        exit 1
    fi
    case "$v" in
        $e )
            ;;
        $a )
            echo "devtool:WARNING: $t version $v accepted, but expected $e." 1>&2
            ;;
        * )
            echo "devtool:ERROR: $t version $v NOT acceptable, requires $e." 1>&2
            exit 1
            ;;
    esac
    echo "$v"
}

devtool_autogen () {
    tool=$1
    shift
    case $tool in
        autoconf )
            autoconf_version=`devtool_require autoconf --version 4 "$1" "$2"`
            echo "generating (GNU Autoconf $autoconf_version): configure config.h.in"
            autoconf
            autoheader 2>&1 | grep -v "is unchanged"
            rm -rf autom4te.cache >/dev/null 2>&1
            ;;
        libtool )
            libtoolize_version=`devtool_require libtoolize --version 4 "$1" "$2"`
            echo "generating (GNU Libtool $libtoolize_version): ltmain.sh, libtool.m4, config.guess, config.sub"
            libtoolize --force --copy >/dev/null 2>&1
            cp `libtoolize --force --copy --dry-run | grep "add the contents of" |\
                sed -e 's;^[^\`]*\`;;' -e "s;'.*;;"` libtool.m4
            ;;
        shtool )
            shtoolize_version=`devtool_require shtoolize -v 3 "$1" "$2"`
            echo "generating (GNU Shtool $shtoolize_version): shtool"
            shift
            shift
            shtoolize -q "$@"
            ;;
    esac
}

devtool_autoclean () {
    tool=$1
    shift
    case $tool in
        autoconf )
            echo "removing: configure config.h.in"
            rm -f configure config.h.in
            ;;
        libtool )
            echo "removing: ltmain.sh libtool.m4 config.guess config.sub"
            rm -f ltmain.sh libtool.m4 config.guess config.sub
            ;;
        shtool )
            echo "removing: shtool"
            rm -f shtool
            ;;
    esac
}

